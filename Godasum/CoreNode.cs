namespace Godasum {
	using Godot;
	using System;

	internal class CoreNode : Node
	{

		public override void _Ready() {
			GD.Print("Creating Core node.");
			Godasum.Core.initialize();
		}
	}
}
