namespace Godasum.Objects.Battle.Services {
	public class FlameDebuff : BattleService
	{

		private Aspects.Health _health;

		public override void Init() {
			this._health = this.BattleObject.Aspects.Demand<Aspects.Health>();
		}

		public override void PhysicsProcess(float delta) {
			this._health.Damage(1);
		}
	}
}
