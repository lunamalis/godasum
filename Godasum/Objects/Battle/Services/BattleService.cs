namespace Godasum.Objects.Battle.Services {
	public class BattleService : Util.Objects.AbstractService
	{
		public BattleObject BattleObject;

		public void InitializeService(Util.Objects.ServiceContainer container, BattleObject bo) {
			base.InitializeService(container);
			this.BattleObject = bo;
		}
	}
}
