namespace Godasum.Objects.Battle {
	using System;
	using System.Linq;
	using System.ComponentModel.Design;
	using System.Collections.Generic;

	public abstract class BattleObject {

		public int WorldId;

		public Util.Objects.AspectContainer Aspects = new Util.Objects.AspectContainer();

		public Util.Objects.ServiceContainer Services = new Util.Objects.ServiceContainer();

		public virtual void Init() { }

		public virtual void Create() { }

		public virtual void Destroy() { }

	}
}
