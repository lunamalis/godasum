namespace Godasum.Objects.Battle {
	using Godot;

	public abstract class ShipBO : EntityBO
	{

		public abstract string ScenePath { get; }

		public RigidBody2D Node;

		public Sprite Sprite;

		protected Aspects.SpriteRigidBody _SpriteBody;

		protected string _ScenePath;

		public ShipBO() {
			this._ScenePath = this.ScenePath;
			this._SpriteBody = this.Aspects.Add<Aspects.SpriteRigidBody>();
			this._SpriteBody.ScenePath = this._ScenePath;

		}

		public void Spawn() {
			this.Aspects.Start();
			this.Services.Start();

			this.Node = this._SpriteBody.Body;
			this.Sprite = this._SpriteBody.Sprite;
		}

		public void Despawn() {
			this.Aspects.Stop();
			this.Services.Stop();
		}

	}
}
