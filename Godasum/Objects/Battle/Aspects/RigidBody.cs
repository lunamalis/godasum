namespace Godasum.Objects.Battle.Aspects {
	using Godot;

	public class RigidBody : BattleAspect
	{

		public RigidBody2D body;

		public override void Init() {
			this.body = new RigidBody2D();

			var bodyshape = new CollisionShape2D();
			var shape = new RectangleShape2D();

			shape.Extents = new Vector2(50, 50);

			bodyshape.Shape = shape;
			this.body.AddChild(bodyshape);
		}


	}
}
