namespace Godasum.Objects.Battle.Aspects {
	public class Health : BattleAspect
	{

		public float Value;

		public override void Init() {
			this.Value = 1;
		}

		public void Damage(float amount) {
			this.Value -= amount;
		}
	}
}
