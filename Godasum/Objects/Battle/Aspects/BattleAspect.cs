// call super.initialize_aspect, reference battleobject, extend abstractaspect
namespace Godasum.Objects.Battle.Aspects {
	public class BattleAspect : Godasum.Util.Objects.AbstractAspect
	{
		public BattleObject BattleObject;

		public void InitializeAspect(Util.Objects.AspectContainer container, BattleObject bo) {

			this.BattleObject = bo;
			base.InitializeAspect(container);
		}
	}
}
