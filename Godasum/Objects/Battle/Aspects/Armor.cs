namespace Godasum.Objects.Battle.Aspects {
	public class Armor : BattleAspect
	{

		private Health _health;

		public override void Init() {
			this._health = this.BattleObject.Aspects.Demand<Health>();
		}
	}
}
