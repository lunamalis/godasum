namespace Godasum.Objects.Battle.Aspects {
	public interface IDirectionalController
	{

		void Forward();

		void Backward();

		void Left();

		void Right();
	}
}
