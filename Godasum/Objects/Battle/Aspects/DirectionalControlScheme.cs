namespace Godasum.Objects.Battle.Aspects {
	using System;
	using Godot;

	public class DirectionalControlScheme : BattleAspect
	{
		public IDirectionalController Controller;

		public override void Input(InputEvent evt) {
			if (evt is InputEventKey event_key) {
				if (event_key.IsActionPressed("ui_up")) {
					this.Controller.Forward();
				}
				else if (event_key.IsActionPressed("ui_down")) {
					this.Controller.Backward();
				}

				if (event_key.IsActionPressed("ui_left")) {
					this.Controller.Left();
				}
				else if (event_key.IsActionPressed("ui_right")) {
					this.Controller.Right();
				}
			}
		}
	}
}
