namespace Godasum.Objects.Battle.Aspects {
	using System.Diagnostics;
	using Godot;

	public class SpriteAbstractBody<TBodyType> : BattleAspect
	where TBodyType : PhysicsBody2D
	{

		public string ScenePath;

		public TBodyType Body;

		public Sprite Sprite;

		private PackedScene _scene;

		public override void Start() {
			Debug.Assert(this.ScenePath != null, "You forgot to set your scene path.");
			this._scene = ResourceLoader.Load<PackedScene>(this.ScenePath);

			var instance = (TBodyType)this._scene.Instance();

			this.Body = instance;
			this.Sprite = instance.GetNode<Sprite>("Sprite");
		}
	}
}
