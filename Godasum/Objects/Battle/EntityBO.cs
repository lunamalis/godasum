namespace Godasum.Objects.Battle {
	/**
	* <summary>
	* Entities are typically moving objects that interact with the world, or are
	* interacted with.
	* </summary>
	*/
	public abstract class EntityBO : BattleObject {

		public abstract string EntityId { get; }

	}
}
