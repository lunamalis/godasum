namespace Godasum {
	using System.Reflection;
	using System;

	public static class VersionHandler {

		public static int Major { get; private set; }

		public static int Minor { get; private set; }

		public static int Patch { get; private set; }

		public static int Revision { get; private set; }

		public static string Version { get; private set; }

		public static string FullVersion { get; private set; }

		static VersionHandler() {
			init();
		}

		internal static void init() {
			(Version assembly_version, DateTime assembly_datetime) = _get_assembly_version();
			var compiled_datetime = _get_compiled_version();

			// Probably unnecessary, but it's a sanity check.
			var diff = Math.Abs((assembly_datetime - compiled_datetime).TotalSeconds);
			if (diff > 5) {
				throw new ApplicationException($"Conflicting version information. Difference is greater than 5 seconds. Assembly datetime: `{assembly_datetime}`. Compiled datetime: `{compiled_datetime}`.");
			}

			string[] godot_version_data = _get_godot_version_data().Split(new char[] { '.' }, 3);

			VersionHandler.Major = int.Parse(godot_version_data[0]);
			VersionHandler.Minor = int.Parse(godot_version_data[1]);
			VersionHandler.Patch = int.Parse(godot_version_data[2]);
			VersionHandler.Revision = assembly_version.Revision;

			VersionHandler.Version = $"{VersionHandler.Major}.{VersionHandler.Minor}.{VersionHandler.Patch}";
			VersionHandler.FullVersion = $"{VersionHandler.Major}.{VersionHandler.Minor}.{VersionHandler.Patch}-{VersionHandler.Revision}";
		}

		private static (Version assembly_version, DateTime compiled_version) _get_assembly_version() {
			Assembly? curr_assembly = Assembly.GetExecutingAssembly();
			if (curr_assembly == null) {
				throw new ApplicationException("Could not load current assembly.");
			}

			Version? version = curr_assembly.GetName().Version;
			if (version == null) {
				throw new ApplicationException("Could not load current assembly version information.");
			}

			var date = new DateTime(2000, 1, 1).AddDays(version.Build).AddSeconds(version.Revision * 2).ToUniversalTime();

			return (version, date);
		}

		private static DateTime _get_compiled_version() {
			return new DateTime(Godasum.CompileData.CompileTime, DateTimeKind.Utc);
		}

		private static string _get_godot_version_data() {
			return (string)Godot.ProjectSettings.GetSetting("application/config/Godasum/version");
		}

	}
}
