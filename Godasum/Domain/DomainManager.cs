namespace Godasum.Domain {
	using System.Collections.Generic;
	using System;

	public class DomainManager
	{

		public Dictionary<string, Domain> domains = new Dictionary<string, Domain>();

		public void register_domain(string domain) {
			if (this.domains.ContainsKey(domain)) {
				throw new ArgumentException("Domain already exists.");
			}
			this.domains.Add(domain, new Domain(domain));
		}

		public dynamic get_resource(string[] domains, string resource_name) {
			if (domains.Length < 0) {
				throw new ArgumentException("No domain given.");
			}
			var last = domains[domains.Length - 1];

			if (!this.domains.ContainsKey(last)) {
				throw new ArgumentException("Given domain does not exist.");
			}

			return this.domains[last].get_resource(Godasum.Util.ArrayUtil.GetAllButLast(domains), resource_name);
		}

	}
}
