namespace Godasum.Domain {
	using System;
	using System.Collections.Generic;

	public class Domain {

		public string value;

		public Dictionary<string, Domain> subdomains = new Dictionary<string, Domain>();
		public Dictionary<string, dynamic> resources = new Dictionary<string, dynamic>();

		public Domain(string value) {
			this.value = value;
		}

		public void register_subdomain(string subdomain) {
			if (this.subdomains.ContainsKey(subdomain)) {
				throw new ArgumentException("Subdomain already exists.");
			}
			this.subdomains.Add(subdomain, new Domain(subdomain));
		}

		public dynamic get_resource(string[] domains, string resource_name) {
			if (domains.Length < 0) {
				// Then they are wanting this domain.
				return this.get_resource(resource_name);
			}

			var last = domains[domains.Length - 1];

			if (!this.subdomains.ContainsKey(last)) {
				throw new ArgumentException("Given subdomain does not exist.");
			}

			return this.subdomains[last].get_resource(Godasum.Util.ArrayUtil.GetAllButLast(domains), resource_name);
		}

		public dynamic get_resource(string resource_name) {
			if (!this.resources.ContainsKey(resource_name)) {
				return this.resources[resource_name];
			}
			else {
				throw new ArgumentException("Resource not found.");
			}
		}

	}
}
