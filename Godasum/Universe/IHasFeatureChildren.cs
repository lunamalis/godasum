namespace Godasum.Universe {
	public interface IHasFeatureChildren
	{

		IUniverseFeature[] Features { get; set; }

	}
}
