namespace Godasum.Universe {
	public interface ILocus : IUniverseFeature
	{

		void Interact();

	}
}
