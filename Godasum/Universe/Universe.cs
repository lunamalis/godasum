namespace Godasum.Universe {
	using System;
	using System.IO;
	using Newtonsoft.Json;

	[JsonObject(MemberSerialization.OptIn)]
	public class Universe
	{

		[JsonProperty("data")]
		public UniverseData Data { get; set; }

		[JsonProperty("currentLocation")]
		public int CurrentLocation { get; set; }

		public string Serialize(Formatting formatting = Formatting.Indented) {
			var settings = UniverseData.GetJsonSettings(Core.Registry.UniverseFeatureRegistry.Types);
			return JsonConvert.SerializeObject(this, formatting, settings);
		}

		public void Save(string path) {
			var text = this.Serialize();
			File.WriteAllText(path, text);
		}

		public static Universe Load(string path) => Deserialize(File.ReadAllText(path));

		public static Universe Deserialize(string inputtext) {
			Godot.GD.Print("Deserializing, getting JSON settings");
			var settings = UniverseData.GetJsonSettings(Core.Registry.UniverseFeatureRegistry.Types);
			Godot.GD.Print(settings);
			var returner = JsonConvert.DeserializeObject<Universe>(inputtext, settings);
			if (returner is null) {
				throw new ArgumentException("Couldn't deserialize universe data.");
			}

			return returner;
		}

	}
}
