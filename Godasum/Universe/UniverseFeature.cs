namespace Godasum.Universe {
	using Newtonsoft.Json;

	[JsonObject(MemberSerialization.OptIn)]
	public abstract class UniverseFeature : IUniverseFeature
	{

		public Universe Universe { get; set; }

		public UniverseData UniverseData { get; set; }

		[JsonProperty("instanceId")]
		public int InstanceId { get; set; }

		public abstract void Spawn(Godot.Node parent);

	}
}
