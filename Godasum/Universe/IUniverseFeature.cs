namespace Godasum.Universe {
	public interface IUniverseFeature
	{

		Universe Universe { get; set; }

		UniverseData UniverseData { get; set; }

		// Used for tracking player location, always unique
		// TODO: would it be better for this to be a GUID?
		int InstanceId { get; set; }

		void Spawn(Godot.Node parent);

	}
}
