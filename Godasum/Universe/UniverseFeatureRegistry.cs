namespace Godasum.Universe {
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;

	public sealed class UniverseFeatureRegistry
	{

		public HashSet<Type> Types { get; } = new HashSet<Type>();

		public void Register<T>()
		where T : IUniverseFeature {
			this.Types.Add(typeof(T));
		}

		public void Register(Type type) {
			Debug.Assert(type.IsSubclassOf(typeof(IUniverseFeature)));
			this.Types.Add(type);
		}

		public void Clear() {
			throw new NotImplementedException();
		}

		public bool IsRegistered<T>()
		where T : IUniverseFeature {
			return this.IsRegistered(typeof(T));
		}

		public bool IsRegistered(Type type) {
			Debug.Assert(type.IsSubclassOf(typeof(IUniverseFeature)));

			return this.Types.Contains(type);
		}

	}
}
