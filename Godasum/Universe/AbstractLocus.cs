namespace Godasum.Universe {
	using Newtonsoft.Json;

	public abstract class AbstractLocus : UniverseFeature, ILocus
	{

		[JsonProperty("position")]
		public Godot.Vector2 Position { get; set; }

		public delegate void OnInteractHandler();
		public event OnInteractHandler OnInteract;

		// Loci are intended to override this.
		// This could potentially be called by a script, or by the locus
		// when the user clicks.
		public virtual void Interact() {
			this.OnInteract?.Invoke();
		}

	}
}
