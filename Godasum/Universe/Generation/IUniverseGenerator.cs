namespace Godasum.Universe.Generation {
	using System;

	public interface IUniverseGenerator
	{

		Universe Universe { get; set; }

		UniverseData Generate(Random random);
	}
}
