namespace Godasum.Universe.Generation.Sample {
	using System;

	internal sealed class DefaultUniverseGenerator : AbstractUniverseGenerator
	{

		public override UniverseData Generate(Random random) {
			return UniverseData.Empty;
		}
	}
}
