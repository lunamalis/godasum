namespace Godasum.Universe.Generation {
	using System;

	public abstract class AbstractUniverseGenerator : IUniverseGenerator
	{

		public Universe Universe { get; set; }

		private int _current_id = 0;

		public abstract UniverseData Generate(Random random);

		public T CreateFeature<T>()
		where T : IUniverseFeature, new() {
			var t = new T();
			return this.CreateFeature(t);
		}

		public T CreateFeature<T>(T t)
		where T : IUniverseFeature {
			t.InstanceId = this._current_id++;
			t.Universe = this.Universe;
			t.UniverseData = this.Universe.Data;
			return t;
		}


	}
}
