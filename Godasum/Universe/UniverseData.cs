namespace Godasum.Universe {
	using System;
	using System.Collections.Generic;
	using Newtonsoft.Json;

	[JsonObject(MemberSerialization.OptIn)]
	public sealed class UniverseData : IHasFeatureChildren
	{

		[JsonProperty("features")]
		public IUniverseFeature[] Features { get; set; } = new IUniverseFeature[0];

		public static UniverseData Empty => new UniverseData();

		public static JsonSerializerSettings GetJsonSettings(IEnumerable<Type> types) {
			var s = new JsonSerializerSettings();
			var converter = JsonSubTypes.JsonSubtypesConverterBuilder.Of(typeof(IUniverseFeature), "featureType");

			foreach (var type in types) {
				// TODO: as a key, instead of using the fullname, we should allow types to supply
				// their own identifier.
				converter = converter.RegisterSubtype(type, type.FullName);
			}

			// converter.SetFallbackSubtype

			var end_converter = converter.SerializeDiscriminatorProperty().Build();
			s.Converters.Add(end_converter);
			return s;
		}

	}
}
