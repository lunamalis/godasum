namespace Godasum.Modding {

	using System.Collections.Generic;
	using Newtonsoft.Json;

	[JsonObject(MemberSerialization.OptOut, MissingMemberHandling=MissingMemberHandling.Error)]
	public class ModInfo
	{
		/**
		* <summary>
		* The Mod ID. This is your "primary" domain. It should be in
		* all-lowercase, contain only alphanumeric characters and underscores, and
		* should not begin or end with an underscore.
		* </summary>
		*/
		[JsonProperty("modid")]
		public string id;

		/**
		* <summary>
		* The mod loader you would like to use.
		*
		* By default, you should use `"gml"` (Godasum mod loader).
		*
		* While the Godasum mod loader initially loads all mods, if the mod
		* requests a different modloader it will pass off the mod to that
		* modloader for them to deal with.
		*
		* To specify another modloader, you must specify their `mod_id`.
		* </summary>
		*/
		public string loader;

		/**
		* <summary>
		* The version of the mod loader you specified.
		* </summary>
		*/
		public string loader_version;

		/**
		* <summary>
		* What type of mod you are using.
		*
		* You are probably wanting a normal mod, not a core mod.
		* </summary>
		*/
		[JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
		public ModType type;

		/**
		* <summary>
		* The version of the mod.
		* </summary>
		*/
		public string version;

		/**
		* <summary>
		* The PascalCase version of the modid.
		* </summary>
		*/
		public string cname;

		/**
		* <summary>
		* The "friendly" name of the mod.
		* </summary>
		*/
		public string name;

		/**
		* <summary>
		* The path to a logo file. It should be a .png.
		*
		* Optional. If not provided, one will be provided.
		* </summary>
		*/
		[JsonProperty(Required=Required.DisallowNull)]
		public string logo_path;

		/**
		* <summary>
		* The license of your mod.
		*
		* Optional. If your mod is not under a license, simply do not provide
		* this.
		* </summary>
		*/
		[JsonProperty(Required=Required.DisallowNull)]
		public string license;

		/**
		* <summary>
		* The URL to your repository for your source code.
		*
		* Optional.
		* </summary>
		*/
		[JsonProperty(Required=Required.DisallowNull)]
		public string repo_url;

		/**
		* <summary>
		* The URL to your issues website or webpage.
		*
		* This is where users should be linked to when they experience a bug in your mod.
		*
		* Optional.
		* </summary>
		*/
		[JsonProperty(Required=Required.DisallowNull)]
		public string issues_url;

		/**
		* <summary>
		* A brief description of the authors.
		* </summary>
		*/
		[JsonProperty(Required=Required.DisallowNull)]
		public string author_info;

		/**
		* <summary>
		* A very short description of your mod.
		* </summary>
		*/
		public string summary;

		/**
		* <summary>
		* A longer description of your mod.
		*
		* Optional.
		* </summary>
		*/
		[JsonProperty(Required=Required.DisallowNull)]
		public string description;

		/**
		* <summary>
		* A list of extra domains that you would like access to.
		*
		* If two mods attempt to get the same domain, the one that is loaded first
		* will be given it.
		*
		* Optional.
		* </summary>
		*/
		[JsonProperty(Required=Required.DisallowNull)]
		public string[] domains;

		/**
		* <summary>
		* A list of dependencies your mod needs.
		*
		* Optional.
		* </summary>
		*/
		[JsonProperty(Required=Required.DisallowNull)]
		public Dictionary<string, ModDependency> dependencies;

		/**
		* <summary>
		* If your mod is a core mod, specify your overrides here.
		*
		* This is not currently implemented. It is possible it will use Harmony or
		* a similar library.
		*
		* If two mods try to override the same thing, an exception will occur.
		*
		* You need to specify in this format:
		* ```
		* "core_overrides": {
		* 	"Godasum.some.thing.to.override": "MyMod.my.override"
		* }
		* ```
		*
		* You are able to override private, protected, and internal entries.
		* Beware however that these are often nonpublic for a reason. Their existence and their behavior may change from update to update without warning.
		*
		* Optional.
		* </summary>
		*/
		[JsonProperty(Required=Required.DisallowNull)]
		public Dictionary<string, string> core_overrides;

}
}
