namespace Godasum.Modding {
	using System;

	public abstract class Mod {
		public abstract string MODID { get; }

		public Godasum.Modloader.ModLoader ModLoader;

		public Mod() { }

		public void Init(Godasum.Modloader.ModLoader modloader) {
			this.ModLoader = modloader;
		}

		public virtual void Setup() { }

		public virtual void QueueMessages() { }

		public virtual void ProcessMessages() { }

		public void RegisterEntity<T>(string entity_id)
		where T : Godasum.Objects.Battle.EntityBO, new() {
			Godot.GD.Print($"Mod class: register_entity: {entity_id}, {typeof(T)}");
			this.ModLoader.RegisterEntity<T>(entity_id);
		}

		// public void register_ship<T>(string entity_id)
		// where T : Godasum.objects.Ship, new() {
		// 	this.modloader.register_ship<T>(entity_id);
		// }

		// public void register_projectile<T>(string id)
		// where T : Godasum.objects.Projectile, new() {
		// 	this.modloader.register_projectile<T>(id);
		// }
	}
}
