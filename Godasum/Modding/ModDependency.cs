namespace Godasum.Modding {
	using Newtonsoft.Json;

	[JsonObject(MemberSerialization.OptOut, MissingMemberHandling=MissingMemberHandling.Error)]
	public class ModDependency
	{
		public string mod_id;

		public string version;

		[JsonProperty(Required=Required.DisallowNull)]
		public bool optional = false;

	}
}
