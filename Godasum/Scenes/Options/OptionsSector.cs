namespace Godasum.Scenes.Options {
	public class OptionsSector {

		private readonly Scenes.Options.Options _parent;

		private readonly string _name;

		public OptionsSector(Godasum.Scenes.Options.Options parent, string name) {
			this._parent = parent;
			this._name = name;
		}

	}
}
