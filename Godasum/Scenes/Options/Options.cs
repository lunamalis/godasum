namespace Godasum.Scenes.Options {
	using System.Collections.Generic;
	using Godot;
	using System;

	public class Options : Node2D
	{

		private Rect2 _screen_size;

		private Node2D _sector_selector;
		private Node2D _sector_leave;
		private Node2D _sector_options;

		private Button _button_video;

		private readonly float _window_titlebar_beight = 3;

		private Node2D? _current_options;

		// Works in percentages.
		private readonly Dictionary<string, Vector2[]> _sector_dimensions = new Dictionary<string, Vector2[]> {
			{ "SectorSelector", new Vector2[2] { default, new Vector2(10, 90) } },
			{ "SectorLeave", new Vector2[2] { new Vector2(0, 90), new Vector2(10, 10) } },
			{ "SectorOptions", new Vector2[2] { new Vector2(10, 0), new Vector2(90, 100) } }
		};

		// private readonly List<OptionsSector> _sectors = new List<OptionsSector>();

		private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

		public override void _Ready() {
			this._screen_size = this.GetViewportRect();

			this._get_nodes();

			// this._sectors.Add(new OptionsSector(this, "SectorSelector"));

			this.GetTree().Root.Connect("size_changed", this, "_align_nodes");
			this._align_nodes();

			log.Info(this._sector_leave.GetNode<Button>("LeaveButton").RectPosition);
			log.Info(this._sector_leave.GetNode<Button>("LeaveButton").RectGlobalPosition);
			// var lbutton = this._sector_leave.GetNode<Button>("LeaveButton");
			// this._sector_leave.MoveChild(lbutton, 1);
			// lbutton.RectPosition = new Vector2(30, 150);
			// GD.Print(this._sector_leave.GetNode<Button>("LeaveButton").RectPosition);
		}

		private void _get_nodes() {
			this._sector_selector = this.GetNode<Node2D>("SectorSelector");
			this._sector_leave = this.GetNode<Node2D>("SectorLeave");
			this._sector_options = this.GetNode<Node2D>("SectorOptions");

			this._button_video = this._sector_selector.GetNode<Button>("VideoButton");
		}

		private void _align_nodes() {
			this._mk_sector(this._sector_selector, new Color(0, 0, 0, 1));
			this._mk_sector(this._sector_leave, new Color(0.1f, 0.1f, 0.1f, 1));
			this._mk_sector(this._sector_options, new Color(0.35f, 0.35f, 0.4f, 1));

			// // Center the selector sector's buttons.
			// var selector_children = this._sector_selector.GetChildren();
			// for (int i = 0; i < selector_children.Count; i++) {
			// 	if (!(selector_children[i] is Button)) { continue; }

			// 	var btn = (Button)selector_children[i];
			// 	var sdim = this._sector_dimensions[this._sector_selector.Name];
			// 	btn.RectPosition = new Vector2(
			// 		this._screen_size.Size.x * sdim[1].x / 100 / 2 - btn.RectSize.x / 2,
			// 		20 * i
			// 	);
			// }
		}

		private void _mk_sector(Node2D sector, Color color) {
			var dim = this._sector_dimensions[sector.Name];
			log.Info($"{sector.Name}");
			foreach (var c in sector.GetChildren()) { log.Info($"\t{c}"); }

			// sector.Position = this._screen_size.Size.x * (dim[0] / 100);
			// sector.Position += new Vector2(0, this._window_titlebar_beight);
			sector.Position = new Vector2(
				this._screen_size.Size.x * (dim[0].x / 100),
				this._screen_size.Size.y * (dim[0].y / 100) + this._window_titlebar_beight
			);

			var color_rect = new ColorRect();
			color_rect.SetSize(
				new Vector2(
					this._screen_size.Size.x * (dim[1].x / 100),
					this._screen_size.Size.y * (dim[1].y / 100)
				)
			);
			color_rect.Color = color;

			sector.AddChild(color_rect);
			// Place the color rect at the beginning, so that we don't hide other elements.
			sector.MoveChild(color_rect, 0);

			// Center the sector's buttons.
			// (The options-sector has no nodes.)
			var sector_children = sector.GetChildren();
			for (int i = 0; i < sector_children.Count; i++) {
				if (!(sector_children[i] is Button)) { continue; }

				var btn = (Button)sector_children[i];
				log.Info($"sector name: {sector.Name}, spos: {sector.Position}, btn: {btn.Name}, bpos: {btn.RectPosition}, bgpos: {btn.RectGlobalPosition}");
				btn.RectPosition = new Vector2(
					this._screen_size.Size.x * dim[1].x / 100 / 2 - btn.RectSize.x / 2,
					// 2 * i + this._screen_size.Size.y * dim[0].y / 100
					10 * i
				);
			}
		}

		private void _enter_options(string name) {
			var pkd_scene = (PackedScene)ResourceLoader.Load($"res://Godasum/scenes/options/{name}.tscn");
			this._current_options = (Node2D)pkd_scene.Instance();
			this._sector_options.AddChild(this._current_options);
		}

		// A null-check is required before calling this function.
		private void _exit_options() {
			this._sector_options.RemoveChild(this._current_options!);
			this._current_options!.QueueFree();
		}

		private void _change_options(string name) {
			if (this._current_options != null) {
				this._exit_options();
			}

			this._enter_options(name);
		}

		public void _on_LeaveButton_pressed() {
			this.GetTree().ChangeScene("res://Godasum/scenes/title/Title.tscn");
		}

		public void _on_VideoButton_pressed() {
			this._change_options("OptionsVideo");
		}

	}
}
