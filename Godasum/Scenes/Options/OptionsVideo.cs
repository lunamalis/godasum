namespace Godasum.Scenes.Options {

	using Godot;
	using System;
	using System.Runtime.Serialization;

	public class OptionsVideo : Node2D
	{

		private OptionButton _resolution_button;
		private CheckBox _borderless_check;
		private CheckBox _fullscreen_check;
		private Node2D _prompt;

		private Config.UserConfigSettings _previous_settings = Core.configs.user.Settings;
		private bool _prompting = false;
		private bool _prompted_ok = false;

		public override void _Ready() {
			this._get_nodes();

			// TODO: this needs to respond to resize events.
			var prompt_bg = this._prompt.GetNode<ColorRect>("ColorRect");
			prompt_bg.RectSize = this.GetViewportRect().Size;

			this._setup();
		}

		private void _get_nodes() {
			this._resolution_button = this.GetNode<OptionButton>("ResolutionButton");
			this._borderless_check = this.GetNode<CheckBox>("Borderless");
			this._fullscreen_check = this.GetNode<CheckBox>("Fullscreen");

			this._prompt = this.GetNode<Node2D>("Prompt");
		}

		private void _setup() {
			this._setup_resolution_button();
			this._setup_borderless();
			this._setup_fullscreen();
		}

		private async void _prompt_ok() {
			if (this._prompting) {
				throw new InvalidOperationException("Cannot start a prompt while it is already prompting.");
			}
			this._prompting = true;
			this._prompted_ok = false;

			this._prompt.Visible = true;

			const int time = 5;
			var timer = this.GetTree().CreateTimer(time);
			await this.ToSignal(timer, "timeout");

			if (this._prompted_ok) {
				// They clicked "okay" so we have nothing more to do.
				return;
			}

			// Otherwise, we need to reset to older settings.
			this._prompting = false;
			this._prompted_ok = false;

			this._prompt.Visible = false;

			Core.configs.user.Settings = this._previous_settings;
			Core.configs.user.Apply();

			// Reset options
			this._setup();

		}

		protected internal void _on_prompt_OKButton_pressed() {
			this._prompting = false;
			this._prompted_ok = true;

			this._prompt.Visible = false;

			Core.configs.user.Save();
			Core.configs.user.Apply();
		}

		private void _setup_resolution_button() {
			this._resolution_button.Clear();

			var resolution_options = new[] {
				"1024x600",
				"1920x1080",
				"1280x720",
				"640x360",
				"16x16"
			};

			// Add current one, if not in default.
			var current_resolution = $"{Core.configs.user.Settings.Display.Dimensions.x}x{Core.configs.user.Settings.Display.Dimensions.y}";
			var has_current = false;
			foreach (var reso in resolution_options) {
				if (reso == current_resolution) {
					has_current = true;
					break;
				}
			}

			if (!has_current) {
				this._resolution_button.AddItem(current_resolution);
			}

			foreach (var reso in resolution_options) {
				this._resolution_button.AddItem(reso);
			}

			// The Items array seems to have 5 times as many items as we want.
			const int ARRAY_MULTI_SIZE = 5;
			var index = this._resolution_button.Items.IndexOf(current_resolution) / ARRAY_MULTI_SIZE;

			this._resolution_button.Select(index);

		}

		protected internal void _on_ResolutionButton_item_selected(int id) {
			this._previous_settings = this._deep_clone_object(Core.configs.user.Settings);
			string res_text = this._resolution_button.GetItemText(id);

			// Core.configs.user.Settings.Display.Dimensions = res_text;

			var splitted = res_text.Split(new[] { 'x' }, 2);
			Core.configs.user.Settings.Display.Dimensions = new Vector2(
				int.Parse(splitted[0]),
				int.Parse(splitted[1])
			);

			this._prompt_ok();
			Core.configs.user.Apply();

		}

		private void _setup_borderless() {
			this._borderless_check.Pressed = Core.configs.user.Settings.Display.Borderless;
		}

		protected internal void _on_Borderless_toggled(bool pressed) {
			this._previous_settings = this._deep_clone_object(Core.configs.user.Settings);

			Core.configs.user.Settings.Display.Borderless = pressed;

			this._prompt_ok();
			Core.configs.user.Apply();

		}

		private void _setup_fullscreen() {
			this._fullscreen_check.Pressed = Core.configs.user.Settings.Display.Fullscreen;
		}

		protected internal void _on_Fullscreen_toggled(bool pressed) {
			this._previous_settings = this._deep_clone_object(Core.configs.user.Settings);

			Core.configs.user.Settings.Display.Fullscreen = pressed;

			this._prompt_ok();
			Core.configs.user.Apply();
		}

		// Yeah, this is really inefficient. But it's not a big deal.
		// Rarely called, only when certain options change.
		private T _deep_clone_object<T>(T obj) {
			using (var stream = new System.IO.MemoryStream()) {
				var bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
				bf.Context = new StreamingContext(StreamingContextStates.Clone);
				bf.Serialize(stream, obj);

				stream.Position = 0;

				return (T)bf.Deserialize(stream);

			}
		}

	}
}
