namespace Godasum.Scenes.Util {
	using Godot;

	internal class HollowStaticRectangle : StaticBody2D
	{

		internal const int WALL_COUNT = 4;

		[Export]
		internal float width = 100;

		[Export]
		internal float height = 100;

		[Export]
		internal float thickness = 10;

		private Node2D[] _walls = new Node2D[WALL_COUNT];

		public override void _Ready() {
			this._setup();
		}

		internal void reset() {
			this._clear();
			this._setup();
		}

		private void _setup() {
			this._add_wall(
				0,
				new Vector2(0, -this.height + this.thickness),
				new Vector2(this.width, this.thickness)
			);
			this._add_wall(
				1,
				new Vector2(0, this.height - this.thickness),
				new Vector2(this.width, this.thickness)
			);

			this._add_wall(
				2,
				new Vector2(-this.width + this.thickness, 0),
				// The horizontal walls handle the corners, so cut the corners here to avoid collisions.
				new Vector2(this.thickness, this.height - this.thickness * 2)
			);
			this._add_wall(
				3,
				new Vector2(this.width - this.thickness, 0),
				new Vector2(this.thickness, this.height - this.thickness * 2)
			);
		}

		private void _add_wall(int index, Vector2 position, Vector2 dimensions) {
			var child = _mk_wall(position, dimensions);
			this._walls[index] = child;
			this.AddChild(child);
		}

		private void _clear() {
			foreach (var wall in this._walls) {
				this.RemoveChild(wall);
				wall.QueueFree();
			}
			this._walls = new Node2D[WALL_COUNT];
		}

		private static StaticBody2D _mk_wall(Vector2 position, Vector2 dimensions) {
			var body = new StaticBody2D();
			var collider = new CollisionShape2D();
			var shape = new RectangleShape2D() {
				Extents = dimensions
			};

			collider.Shape = shape;
			body.AddChild(collider);
			body.Position = position;
			return body;
		}

	}
}
