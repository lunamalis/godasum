namespace Godasum.Scenes.Util {
	using Godot;
	using System;

	internal class BorderedRectangle : Polygon2D
	{

		internal Color outline = new Color(0, 0, 0);
		internal float width = 3.0f;

		public override void _Draw() {
			var poly = this.Polygon;
			for (int i = 1; i < poly.Length; i++) {
				this.DrawLine(poly[i - 1], poly[i], this.outline, this.width);
			}
			this.DrawLine(poly[poly.Length - 1], poly[0], this.outline, this.width);
		}

	}
}
