namespace Godasum.Scenes.Title {
	using System.Collections.Generic;
	using Godot;

	internal class EasterCode : Node
	{

		private readonly KeyList[] _expected_stack = new[] {
			KeyList.Up,
			KeyList.Up,
			KeyList.Down,
			KeyList.Down,
			KeyList.Left,
			KeyList.Right,
			KeyList.Left,
			KeyList.Right,
			KeyList.B,
			KeyList.A
		};

		private int _pos;

		public override void _UnhandledKeyInput(InputEventKey evt) {

			if (evt.IsEcho()) {
				return;
			}

			if (!evt.IsPressed()) {
				return;
			}

			if (evt.Scancode == (uint)this._expected_stack[this._pos]) {
				this._pos++;

				if (this._pos == this._expected_stack.Length) {
					this._pos = 0;
					this._invoke();
				}
			}
			else {
				this._pos = 0;
			}

		}

		internal delegate void CompleteEvent();

		internal event CompleteEvent on_complete;

		internal bool invoked {
			get {
				return this.invoke_count > 0;
			}
		}

		internal int invoke_count { get; private set; }

		private void _invoke() {
			this.on_complete?.Invoke();
			this.invoke_count++;
		}

	}
}
