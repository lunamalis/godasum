namespace Godasum.Scenes.Title {
	using System.Collections.Generic;
	using Godot;

	internal sealed class SplashText : KinematicBody2D
	{

		internal const string COMMENT_CHARACTER = "#";

		private Label _label;
		private CollisionShape2D _collider;
		private EasterCode _easter;

		private Vector2 _velocity = new Vector2(100, 100);
		private Vector2 _direction = new Vector2(1, 1);

		private string[]? _easter_choices;
		private Timer? _easter_timer;

		private DynamicFont _font;

		public override void _Ready() {
			this._label = this.GetNode<Label>("Label");

			this._font = (DynamicFont)this._label.Get("custom_fonts/font");

			this._collider = this.GetNode<CollisionShape2D>("CollisionShape2D");
			this._easter = this.GetNode<EasterCode>("EasterCode");
			this._easter.on_complete += this._on_code_complete;

			this._randomize_text();
			this._randomize_direction();
		}

		public override void _PhysicsProcess(float delta) {
			var vel = this._velocity * this._direction;
			this.MoveAndSlide(vel);

			if (this.GetSlideCount() > 0) {
				var col = this.GetSlideCollision(0);

				if (col != null) {
					this._direction = this._direction.Bounce(col.Normal);
				}
			}
		}

		private void _randomize_text() {
			this._randomize_text(this._get_choices());
		}

		private void _randomize_text(string[] choices) {
			this._label.Text = Godasum.Util.RandomUtil.ChooseRandom(choices);
			this._label.SetSize(this._font.GetStringSize(this._label.Text));
			this._mk_body();
		}

		private string[] _get_choices() {
			var lines = System.IO.File.ReadAllLines(ProjectSettings.GlobalizePath("res://resources/text/splash.txt"));

			var filtered_lines = new List<string>();
			foreach (var line in lines) {
				var stripped = line.Trim();
				if (stripped.BeginsWith(COMMENT_CHARACTER)) { continue; }
				if (stripped.Length < 1) { continue; }
				filtered_lines.Add(stripped);
			}

			return filtered_lines.ToArray();

		}

		private void _randomize_direction() {
			GD.Randomize();
			if (GD.Randf() > 0.5) {
				this._direction.x *= -1;
			}

			if (GD.Randf() > 0.5) {
				this._direction.y *= -1;
			}
		}

		// Anchor is at the top left
		private void _mk_body() {
			var text_size = this._label.RectSize / 2;
			var shape = new RectangleShape2D() {
				Extents = text_size
			};

			this._collider.Shape = shape;
			this._collider.Position = text_size;
		}

		private void _on_Label_mouse_entered() {
			if (!this._easter.invoked) {
				this._randomize_text();
			}
		}

		private void _on_code_complete() {
			if (this._easter.invoked) {
				return;
			}

			this._easter_choices = this._get_choices();
			this._easter_timer = new Timer();

			const float time = 0.075f;
			this._easter_timer.WaitTime = time;

			// Can't wait for Godot 4.0 where we can use real events.
			this._easter_timer.Connect("timeout", this, nameof(this._randomize_easter));

			this.AddChild(this._easter_timer);
			this._easter_timer.Start();

		}

		private void _randomize_easter() {
			this._randomize_text(this._easter_choices!);
		}

	}
}
