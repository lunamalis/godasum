namespace Godasum.Scenes.Title {
	using System.Reflection;
	using System.Collections.Generic;
	using Godot;
	using Godasum;
	using System;
	using Godasum.Scenes.Util;

	public class Title : Node2D
	{

		internal TitleText title_text;
		internal Button play_button;
		internal ColorRect background;
		internal Label version_text;
		internal Button options_button;
		internal Rect2 screen_size;

		private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

		public override void _Ready() {

			Godasum.Core.configs.apply_all();

			Godasum.Core.modloader.load_all();

			this.title_text = (Godasum.Scenes.Title.TitleText)(this.GetNode<Node2D>("TitleText"));
			this.play_button = this.GetNode<Button>("PlayButton");
			this.background = this.GetNode<ColorRect>("Background");
			this.version_text = this.GetNode<Label>("VersionText");
			this.options_button = this.GetNode<Button>("OptionsButton");

			this.screen_size = this.GetViewportRect();

			this._align_nodes();

			var walls = (HollowStaticRectangle)ResourceLoader.Load<PackedScene>("res://Godasum/scenes/util/HollowStaticRectangle.tscn").Instance();
			walls.Position = new Vector2(this.screen_size.Size.x / 2, this.screen_size.Size.y / 2);
			walls.width = this.screen_size.Size.x / 2 + walls.thickness * 2;
			walls.height = this.screen_size.Size.y / 2 + walls.thickness * 2;
			this.AddChild(walls);

			this._set_version();

			log.Info(new DateTime(Godasum.CompileData.CompileTime, DateTimeKind.Utc));
			// this._mk_ships();
		}

		private void _align_nodes() {
			// this.title_text.RectPosition = new Vector2(
			// 	this.screen_size.Size.x / 2 - this.title_text.RectSize.x / 2,
			// 	this.title_text.RectPosition.y
			// );
			log.Info($"w: {this.screen_size.Size.x / 2 - this.title_text.width / 2}, {this.title_text.width / 2}");
			this.title_text.Position = new Vector2(
				this.screen_size.Size.x / 2 - this.title_text.width / 2,
				this.title_text.Position.y
			);

			// this.play_button.RectPosition = new Vector2(
			// 	this.screen_size.x / 2 -
			// );
			Godasum.Util.NodeUtil.CenterNode(
				this.screen_size,
				this.play_button
			);

			Godasum.Util.NodeUtil.CenterNode(
				this.screen_size,
				this.options_button
			);
			this.options_button.RectPosition += new Vector2(0, 35);

			this.background.RectSize = new Vector2(
				this.screen_size.Size.x,
				this.screen_size.Size.y
			);

			this.version_text.RectPosition = new Vector2(
				0,
				this.screen_size.Size.y - this.version_text.RectSize.y
			);
		}

		private void _set_version() {
			// this.version_text.Text = $"version {Godasum.Reference.VERSION}";
			this.version_text.Text = $"Godasum\nversion {Godasum.VersionHandler.Version}\nrevision {Godasum.VersionHandler.Revision}";
			// GD.Print(ProjectSettings.HasSetting("Godasum/Version")); // false??
			// this.version_text.Text = (string)ProjectSettings.GetSetting("Godasum/Version");
		}

		internal void _on_PlayButton_pressed() {
			this.GetTree().ChangeScene("res://Godasum/scenes/starmap/Starmap.tscn");
		}

		internal void _on_OptionsButton_pressed() {
			this.GetTree().ChangeScene("res://Godasum/scenes/options/Options.tscn");
		}

		// private void _mk_ships() {
		// 	var choice = Godasum.util.RandomUtil.choose_random_dict(Godasum.Core.modloader.ship_types);
		// 	var entity = (Godasum.objects.Entity)Activator.CreateInstance(choice);

		// 	for (var i = 0; i < 25; i++) {
		// 		var rb = new RigidBody2D();
		// 		var sp = new Sprite();

		// 		sp.Texture = entity.sprite.Texture;
		// 		sp.RotationDegrees = entity.rotation_sprite;
		// 		// Opacity.
		// 		sp.Modulate = new Color(1, 1, 1, 0.2f);

		// 		sp.Scale = new Vector2(1, 1);

		// 		rb.AddChild(sp);

		// 		rb.Position = new Vector2(GD.Randi() % this.screen_size.Size.x, GD.Randi() % this.screen_size.Size.y);
		// 		rb.RotationDegrees = 90;
		// 		rb.GravityScale = 0;
		// 		rb.ApplyTorqueImpulse(10000);

		// 		this.AddChild(rb);

		// 		// this.splash_bodies.Add(rb);
		// 	}
		// }

	}
}
