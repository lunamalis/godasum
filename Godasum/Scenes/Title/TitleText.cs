namespace Godasum.Scenes.Title {
	using System.Collections.Generic;
	using Godot;
	using System;

	internal class TitleText : Node2D
	{

		[Export]
		internal string Text = "Lunamalis";

		[Export]
		internal float time_period = 0.2f;

		internal float width;
		internal float height;

		internal Timer text_color_timer;

		private Font _font;
		private List<Color> _colors = new List<Color>();

		private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

		public override void _Ready() {

			this._get_font();
			GD.Randomize();
			if (GD.Randf() < 0.01) {
				this.Text = "Lunar Malice";
			}

			this.text_color_timer = this.GetNode<Timer>("TextColorTimer");
			this.text_color_timer.WaitTime = this.time_period;
			this.text_color_timer.Autostart = true;
			this.text_color_timer.Start();

			this._colors = this._interpolate(new Color(0.8f, 0.8f, 0.8f, 1.0f), new Color(1.0f, 1.0f, 1.0f, 1.0f), this.Text.Length);
			this._colors = Godasum.Util.ListUtil.ShiftRight(this._colors);

			var tmp = this._font.GetStringSize(this.Text);
			this.width = tmp.x;
			this.height = tmp.y;
		}

		public override void _Draw() {
			var x_offset = 0f;
			for (var i = 0; i < this.Text.Length; i++) {
				var chara = this.Text[i];

				var next_chara = "";
				if (i + 1 < this.Text.Length) {
					next_chara = this.Text[i + 1].ToString();
				}

				x_offset += this.DrawChar(
					this._font,
					new Vector2(x_offset, this.height),
					chara.ToString(),
					next_chara,
					this._colors[i]
				);
			}
		}

		private void _get_font() {
			var dynfont = new DynamicFont();
			dynfont.FontData = (DynamicFontData)ResourceLoader.Load("res://resources/fonts/WorkSans-Light.otf");
			this._font = dynfont;
		}

		private List<Color> _interpolate(Color a, Color b, int steps) {
			var r1 = a.r;
			var r2 = b.r;
			var b1 = a.b;
			var b2 = b.b;
			var g1 = a.g;
			var g2 = b.g;
			var returner = new List<Color>();
			for (var i = 0; i < steps; i++) {
				float frac = (float)i / (float)steps;
				var result_r = (r2 - r1) * frac + r1;
				var result_g = (g2 - g1) * frac + g1;
				var result_b = (b2 - b1) * frac + b1;
				returner.Add(new Color(result_r, result_g, result_b));
			}
			return returner;
		}

		private void _on_TextColorTimer_timeout() {
			// Not very efficient. However, it only runs 5 times/second. Sue me.
			this._colors = Godasum.Util.ListUtil.ShiftRight(this._colors);
			this.Update();
		}

	}
}
