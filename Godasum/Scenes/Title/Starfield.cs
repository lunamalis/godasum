namespace Godasum.Scenes.Title {
	using Godot;
	using System;

	public class Starfield : Node2D
	{

		private Rect2 _screen_size;

		private Color _color = new Color(1, 1, 1);

		public override void _Ready() {
			this._screen_size = this.GetViewportRect();
		}

		public override void _Draw() {
			for (int i = 0; i < 25; i++) {
				GD.Randomize();
				var x = GD.Randf() * this._screen_size.Size.x;
				var y = GD.Randf() * this._screen_size.Size.y;
				this.DrawCircle(new Vector2(x, y), 2, this._color);
			}
		}

	}
}
