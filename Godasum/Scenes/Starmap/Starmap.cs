namespace Godasum.Scenes.Starmap {
	using System.Diagnostics;
	using Godot;

	public class Starmap : Node2D
	{

		public Universe.Universe Universe { get; set; }

		// Temporary. Mods statically assign this for now.
		public static Universe.Generation.IUniverseGenerator UniverseGenerator { get; set; } = new Universe.Generation.Sample.DefaultUniverseGenerator();

		private Node _layer_loci;

		private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

		public override void _Ready() {
			this._layer_loci = this.GetNode<Node>("LayerLoci");
			log.Info("Starmap readying...");

			log.Info("Building universe...");
			this.Universe = new Universe.Universe() {
				CurrentLocation = 0
			};
			UniverseGenerator.Universe = this.Universe;

			log.Info("Generating universe...");
			var sw = new Stopwatch();
			sw.Start();
			this.Universe.Data = UniverseGenerator.Generate(new System.Random(1234));
			sw.Stop();
			log.Info("Generated universe. ({0})", sw.Elapsed);

			log.Info("Spawning universe.");
			// Now we can spawn each feature into the world.
			// TODO: only do for items on the screen.
			foreach (var feature in this.Universe.Data.Features) {
				recursiveSpawn(this._layer_loci, feature);
			};
			void recursiveSpawn(Node node, Universe.IUniverseFeature feature) {
				feature.Universe = this.Universe;
				feature.UniverseData = this.Universe.Data;

				feature.Spawn(node);
				if (feature is Universe.IHasFeatureChildren parent) {
					foreach (var child in parent.Features) {
						recursiveSpawn(node, child);
					}
				}
			}

			log.Info("Starmap readied.");

			// Dump it to a text for dev.
			System.IO.File.WriteAllText(ProjectSettings.GlobalizePath("user://STARMAP.json"), this.Universe.Serialize());
			// To test that it is the same, deserialize the serialized data.

		}

	}
}
