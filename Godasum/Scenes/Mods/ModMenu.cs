namespace Godasum.Scenes.Mods {
	using Godot;

	public class ModMenu : Node2D
	{
		private ColorRect _background;
		private MarginContainer _center;
		private VBoxContainer _main_container;
		private ScrollContainer _mods_scroll;
		private VBoxContainer _mods_list;

		public override void _Ready() {
			this._background = this.GetNode<ColorRect>("Background");
			this._center = this.GetNode<MarginContainer>("Center");
			this._main_container = this._center.GetNode<VBoxContainer>("Main");
			this._mods_scroll = this._main_container.GetNode<ScrollContainer>("ModsScroll");
			this._mods_list = this._mods_scroll.GetNode<VBoxContainer>("ModsList");

			var viewportrect = this.GetViewportRect();

			this._background.RectSize = viewportrect.Size;

			this._center.RectSize = new Vector2(viewportrect.Size.x * 2 / 3, viewportrect.Size.y);

			this._center.MarginTop = viewportrect.Size.y * 0.1f;
			this._center.MarginLeft = viewportrect.Size.x * 1 / 3;

			foreach (var mod in Core.modloader.mods) {
				this._add_mod();
			}
		}

		private void _add_mod() {
			var node = new Label();
			node.Text = "Mod";
			this._mods_list.AddChild(node);
		}
	}
}
