namespace Godasum.Scenes.Objects.Entity {
	using Godot;
	using System;

	public class EntityNode : RigidBody2D
	{

		// public Godasum.objects.Entity entity;

		public override void _Ready() {
			// this.SetProcess(false);
			this.SetProcessInput(false);

			// this.Connect("body_entered", this, nameof(this._on_BodyEntered));
		}

		public override void _Process(float delta) {
			// this.entity.process();
		}

		public override void _IntegrateForces(Physics2DDirectBodyState state) {
			// this.EmitSignal("integrate_forces", state);
			// this.entity.integrate_forces(state);
		}

		public override void _Input(InputEvent evt) {
			// this.entity.input(evt);
		}

		public void _on_RigidBody2D_body_entered(EntityNode body) {
			// this.entity.on_body_entered(body);
		}
	}
}
