namespace Godasum.Scenes.Battle {
	using Godot;
	using System;

	public class PlayerMarker : Node2D
	{

		private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

		public override void _Draw() {
			log.Info("Drew player marker.");
			this._draw_circle_arc(
				this.Position,
				50,
				0,
				-180,
				new Color(1, 0, 0)
			);
		}

		private void _draw_circle_arc(Vector2 center, float radius, float angleFrom, float angleTo, Color color) {
			int points_n = 32;
			var pointsArc = new Vector2[points_n];

			for (int i = 0; i < points_n; ++i) {
				float anglePoint = Mathf.Deg2Rad(angleFrom + i * (angleTo - angleFrom) / points_n - 90f);
				pointsArc[i] = center + new Vector2(Mathf.Cos(anglePoint), Mathf.Sin(anglePoint)) * radius;
			}

			for (int i = 0; i < points_n - 1; ++i) {
				this.DrawLine(pointsArc[i], pointsArc[i + 1], color);
			}
		}

	}
}
