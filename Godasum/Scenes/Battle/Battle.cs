namespace Godasum.Scenes.Battle {
	using System.Reflection;
	using System.Collections.Generic;
	using Godot;
	using System;

	public class Battle : Node2D
	{

		// public List<Godasum.objects.Entity> entities = new List<Godasum.objects.Entity>();

		internal List<Godasum.Objects.Battle.ShipBO> ships = new List<Godasum.Objects.Battle.ShipBO>();
		// public List<Godasum.objects.Projectile> projectiles = new List<Godasum.objects.Projectile>();

		internal Godasum.Scenes.Battle.PlayerMarker player_marker;
		// public Godasum.objects.Ship player_ship;
		internal Rect2 screen_size;
		internal Rect2 world_rect;
		internal Vector2[] world_rectangle;
		internal Camera2D camera;
		internal ColorRect background;
		internal ColorRect outer_background;
		internal ParallaxBackground parallax;

		private readonly Dictionary<string, Action> _timers = new Dictionary<string, Action>();
		private int _timers_total = 0;

		private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

		public override void _Ready() {
			log.Info("Starting battle.");
			this.camera = this.GetNode<Camera2D>("Camera2D");
			this.RemoveChild(this.camera);
			this.background = this.GetNode<ColorRect>("Background");
			this.outer_background = this.GetNode<ColorRect>("OuterBackground");

			this.screen_size = this.GetViewportRect();
			this.world_rect = new Rect2(new Vector2(0, 0), new Vector2(3000, 3000));

			this.background.RectSize = new Vector2(this.screen_size.Size.x, this.screen_size.Size.y);
			this.outer_background.RectPosition = new Vector2(-this.screen_size.Size.x, -this.screen_size.Size.y);
			this.outer_background.RectSize = new Vector2(this.screen_size.Size.x * 4, this.screen_size.Size.y * 4);

			this.parallax = this.GetNode<ParallaxBackground>("Parallax");
			var parallax_layers = this.parallax.GetChildren();
			foreach (ParallaxLayer p_layer in parallax_layers) {
				foreach (TextureRect p_texture in p_layer.GetChildren()) {
					p_texture.RectPosition = new Vector2(-this.screen_size.Size.x, -this.screen_size.Size.y);
					p_texture.RectSize = new Vector2(this.screen_size.Size.x * 4, this.screen_size.Size.y * 4);
				}
			}

			this.add_ship();

			var player_marker_scene = (PackedScene)ResourceLoader.Load("res://Godasum/scenes/battle/PlayerMarker.tscn");
			// this.player_marker = (Godasum.scenes.battle.PlayerMarker)player_marker_scene.Instance();

			// this.set_player(0);
		}

		internal void add_ship() {
			int i = 1;
			foreach (var kv in Core.modloader.entity_types) {
				var ship_id = kv.Key;
				var ship_type = kv.Value;

				log.Info($"Spawning {ship_id}: {ship_type}");

				var inst = (Godasum.Objects.Battle.ShipBO)Activator.CreateInstance(ship_type);
				inst.Init();
				inst.Spawn();

				inst.Node.Position = new Vector2(
					250f * i,
					50f
				);


				this.AddChild(inst.Node);
				this.ships.Add(inst);

				i++;
			}
		}

		public override void _PhysicsProcess(float delta) {
			// this.world_rect.Position = new Vector2(
			// 	this.player_ship.instance.Position.x - this.world_rect.Size.x / 2,
			// 	this.player_ship.instance.Position.y - this.world_rect.Size.y / 2
			// );
		}

		public override void _Process(float delta) {
		}

		public override void _Input(InputEvent evt) {
			if (evt is InputEventKey inputEventKey) {
				if (inputEventKey.IsActionPressed("player_ship_next")) {
					// this.set_player(this.get_next_alive_ship());
				}
				else if (inputEventKey.IsActionPressed("player_ship_prev")) {
					// this.set_player(this.get_prev_alive_ship());
				}
			}
		}

		// public void add_entity(string name, float x, float y, float rotation) {
		// 	if (!Godasum.Core.modloader.entity_types.ContainsKey(name)) {
		// 		throw new ArgumentException($"No entity named {name}.");
		// 	}

		// 	var ent_type = Godasum.Core.modloader.entity_types[name];

		// 	var e_inst = (Godasum.objects.Entity)Activator.CreateInstance(ent_type);
		// 	e_inst.pre_init(this);
		// 	e_inst.finalize(x, y, rotation, ref this.world_rect);
		// 	this.entities.Add(e_inst);
		// 	this.AddChild(e_inst.instance);
		// }

		// // public void add_projectile(string name, float x, float y) {
		// public void add_projectile(string name, Vector2 pos, float rotation) {
		// 	if (!Godasum.Core.modloader.projectile_types.ContainsKey(name)) {
		// 		throw new ArgumentException($"No projectile named {name}.");
		// 	}

		// 	var proj_type = Godasum.Core.modloader.projectile_types[name];

		// 	var proj_inst = (Godasum.objects.Projectile)Activator.CreateInstance(proj_type);
		// 	proj_inst.pre_init(this);
		// 	proj_inst.finalize(pos.x, pos.y, rotation, ref this.world_rect);
		// 	this.projectiles.Add(proj_inst);
		// 	this.AddChild(proj_inst.instance);
		// }

		internal Transform2D wrap_me(Transform2D transform) {
			var wpos = this.world_rect.Position;
			var wend = this.world_rect.End;

			if (transform.origin.x < wpos.x) {
				transform.origin.x = wend.x;
			}
			else if (transform.origin.x > wend.x) {
				transform.origin.x = wpos.x;
			}

			if (transform.origin.y < wpos.y) {
				transform.origin.y = wend.y;
			}
			else if (transform.origin.y > wend.y) {
				transform.origin.y = wpos.y;
			}

			return transform;
		}

		// public void set_player(int index) {
		// 	var new_ship = this.ships[index];
		// 	if (!new_ship.alive) {
		// 		throw new ArgumentException("Player cannot be set to a ship that is not alive.");
		// 	}
		// 	if (this.player_ship != null) {
		// 		GD.Print("Deactivating player.");
		// 		this._deactivate_player();
		// 	}
		// 	GD.Print("Activating player.");
		// 	this.player_ship = new_ship;

		// 	this._activate_player();
		// }

		// public int get_next_alive_ship() {
		// 	// We have to get the current index. We can't store it because
		// 	// the index might change if new ships are added or old ones
		// 	// are removed.
		// 	var curr_index = this.ships.IndexOf(this.player_ship);
		// 	return this._get_next_living_ship(curr_index);
		// }

		// public int get_prev_alive_ship() {
		// 	var curr_index = this.ships.IndexOf(this.player_ship);
		// 	return this._get_prev_living_ship(curr_index);

		// }

		// public void create_timer(Godasum.objects.Entity entity, Action call, float waittime, bool oneshot = false, bool autostart = true) {
		// 	var timer = new Timer();
		// 	timer.Name = $"_Generated_Timer{this._timers_total}";
		// 	GD.Print($"timer name: {timer.Name}");
		// 	timer.Connect(
		// 		"timeout",
		// 		this,
		// 		nameof(this._timer_callback),
		// 		new Godot.Collections.Array() {
		// 			// We can't pass `call` to this, so we put `call` inside
		// 			// a dictionary and call it using this.
		// 			timer.Name
		// 		}
		// 	);

		// 	timer.WaitTime = waittime;
		// 	timer.OneShot = oneshot;
		// 	timer.Autostart = autostart;
		// 	// this.AddChild(timer);
		// 	this._timers.Add(timer.Name, call);
		// 	this._timers_total++;
		// 	entity.instance.AddChild(timer);
		// 	// timer.Start();
		// }

		// private void _activate_player() {
		// 	this.player_ship.activate_player();
		// 	GD.Print($"camera: `{this.camera}`, marker: `{this.player_marker}`");
		// 	this.player_ship.instance.AddChild(this.camera);
		// 	this.player_ship.instance.AddChild(this.player_marker);
		// }

		// private void _deactivate_player() {
		// 	this.player_ship.deactivate_player();
		// 	this.player_ship.instance.RemoveChild(this.camera);
		// 	this.player_ship.instance.RemoveChild(this.player_marker);
		// }

		private void _timer_callback(string timer_name) {
			// TODO: this should never be null, right?
			this._timers[timer_name]();
		}

		// private int _get_next_living_ship(int index, int tries = 0) {
		// 	if (index >= this.ships.Count) {
		// 		index = 0;
		// 	}
		// 	var next_index = index + 1;
		// 	if (next_index >= this.ships.Count) {
		// 		next_index = 0;
		// 	}

		// 	if (this.ships[next_index].alive) { return next_index; }

		// 	if (tries > (this.ships.Count + 1)) {
		// 		throw new ApplicationException("Could not get next living ship because there are no living ships.");
		// 	}

		// 	return this._get_next_living_ship(next_index, tries++);
		// }

		// private int _get_prev_living_ship(int index, int tries = 0) {
		// 	if (index < 0) {
		// 		index = this.ships.Count;
		// 	}
		// 	var prev_index = index - 1;
		// 	if (prev_index < 0) {
		// 		prev_index = this.ships.Count;
		// 	}

		// 	if (this.ships[prev_index].alive) { return prev_index; }

		// 	if (tries > (this.ships.Count + 1)) {
		// 		throw new ApplicationException("Could not get next living ship because there are no living ships.");
		// 	}

		// 	return this._get_prev_living_ship(prev_index, tries++);
		// }

	}
}
