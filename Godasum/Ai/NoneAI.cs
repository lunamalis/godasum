namespace Godasum.Ai {
	public class NoneAi : AbstractAI {
		public override string ai_id { get { return "none_ai"; } }

		public override void process() {
			// Just do nothing...
			// What did you expect?
		}
	}
}
