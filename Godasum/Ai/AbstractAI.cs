namespace Godasum.Ai {
	public abstract class AbstractAI {
		public abstract string ai_id { get; }

		// public Godasum.objects.Entity entity;

		// public void init(Godasum.objects.Entity entity) {
		// 	this.entity = entity;
		// }

		public abstract void process();
	}
}
