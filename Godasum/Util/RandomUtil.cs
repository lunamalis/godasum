namespace Godasum.Util {
	using System.Collections.Generic;
	using System;

	public static class RandomUtil {

		public static Random rnd = new Random();

		public static T ChooseRandom<T>(T[] arr) {
			return arr[rnd.Next(arr.Length)];
		}

		public static T ChooseRandom<T>(List<T> lst) {
			return lst[rnd.Next(lst.Count)];
		}

		public static TVal ChooseRandomDict<TKey, TVal>(Dictionary<TKey, TVal> dict) {
			var key_list = new List<TKey>(dict.Keys);

			return dict[key_list[rnd.Next(key_list.Count)]];
		}
	}
}
