namespace Godasum.Util {
	using Godot;

	public static class NodeUtil {
		public static void CenterNode(Rect2 rect, Control node) {

			node.RectPosition = new Vector2(
				rect.Size.x / 2 - node.RectSize.x / 2,
				rect.Size.y / 2 - node.RectSize.y / 2
			);

		}
	}

}
