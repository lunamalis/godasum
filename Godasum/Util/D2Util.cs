namespace Godasum.Util {
	using System;
	using Godot;

	// 2D utilities.

	public static class D2Util {
		public enum CLOCK_DIRECTION
		{
			CLOCKWISE = 1,
			COUNTER_CLOCKWISE = 2
		}

		public static CLOCK_DIRECTION ShorterRotationDistance(Vector2 original, float original_direction, Vector2 target) {
			var direction_x = Math.Cos(original_direction);
			var direction_y = Math.Sin(original_direction);
			if (
				direction_y * (original.x - target.x)
				>
				direction_x * (original.y - target.y)
			) {
				return CLOCK_DIRECTION.CLOCKWISE;
			}
			return CLOCK_DIRECTION.COUNTER_CLOCKWISE;
		}
	}
}
