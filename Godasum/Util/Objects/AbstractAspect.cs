namespace Godasum.Util.Objects {
	using System.Diagnostics;
	using System;

	public abstract class AbstractAspect : IObjectComponent, IDisposable
	{

		protected AspectContainer _Container;

		private bool _subscribed_process = false;
		private bool _subscribed_physics_process = false;
		private bool _subscribed_input = false;

		private bool _disposed = false;

		public virtual void InitializeAspect(AspectContainer container) {
			this._Container = container;
			this.Init();
		}

		public virtual void Init() { }

		public virtual void Start() { }

		public virtual void Stop() { }

		public virtual void Process() { }

		public virtual void PhysicsProcess(float delta) { }

		public virtual void Input(Godot.InputEvent evt) { }

		public void SubscribeProcess() {
			Debug.Assert(!this._subscribed_process, "Already subscribed to process.");
			this._subscribed_process = true;
			this._Container.ProcessEvent += this.Process;
		}

		public void UnsubscribeProcess() {
			Debug.Assert(this._subscribed_process, "Can't unsubscribe to process because we aren't subscribed.");
			this._subscribed_process = false;
			this._Container.ProcessEvent -= this.Process;
		}

		public void SubscribePhysicsProcess() {
			Debug.Assert(!this._subscribed_physics_process, "Already subscribed to physics process.");
			this._subscribed_physics_process = true;
			this._Container.PhysicsProcessEvent += this.PhysicsProcess;
		}

		public void UnsubscribePhysicsProcess() {
			Debug.Assert(this._subscribed_physics_process, "Can't unsubscribe to physics process because we aren't subscribed.");
			this._subscribed_physics_process = false;
			this._Container.PhysicsProcessEvent -= this.PhysicsProcess;
		}

		public void SubscribeInput() {
			Debug.Assert(!this._subscribed_input, "Already subscribed to input.");
			this._subscribed_input = true;
			this._Container.InputEvent += this.Input;
		}

		public void UnsubscribeInput() {
			Debug.Assert(this._subscribed_input, "Can't unsubscribe to input because we aren't subscribed.");
			this._subscribed_input = false;
			this._Container.InputEvent -= this.Input;
		}

		public void Dispose() {
			this._dispose(true);
			GC.SuppressFinalize(this);
		}

		// Destroy managed resources that are under .NET's control.
		private void _dispose_managed() {
			// TODO: bad way of doing this?
			if (this._subscribed_process) {
				this.UnsubscribeProcess();
			}

			if (this._subscribed_physics_process) {
				this.UnsubscribePhysicsProcess();
			}

			if (this._subscribed_input) {
				this.UnsubscribeInput();
			}
		}

		// Destroy items that are beyond .NET's control.
		// Like direct file handles, direct network connections, things written in
		// C++, etc.
		private void _dispose_unmanaged() { }

		private void _dispose(bool release_managed) {
			if (this._disposed) {
				return;
			}
			this._disposed = true;

			if (release_managed) {
				this._dispose_managed();
			}

			this._dispose_unmanaged();
		}

		~AbstractAspect() {
			this._dispose(false);
		}
	}
}
