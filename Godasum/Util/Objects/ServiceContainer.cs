namespace Godasum.Util.Objects {
	using System.Diagnostics;
	using System;
	using System.Collections.Generic;
	using System.Linq;

	public class ServiceContainer : IObjectComponentContainer<AbstractService>, IDisposable
	{

		public delegate void ProcessEventHandler();

		public event ProcessEventHandler ProcessEvent;

		public delegate void PhysicsProcessHandler(float delta);

		public event PhysicsProcessHandler PhysicsProcessEvent;

		public delegate void InputHandler(Godot.InputEvent evt);

		public event InputHandler InputEvent;

		private readonly HashSet<AbstractService> _services = new HashSet<AbstractService>();

		private bool _disposed = false;

		/**
		* <summary>
		* Check if a service has already been added.
		* </summary>
		* <typeparam name="T">The type to check for.</typeparam>
		* <returns>If it exists or not.</returns>
		*/
		public bool Has<T>()
		where T : AbstractService {
			foreach (var thing in this._services) {
				if (thing is T) {
					return true;
				}
			}

			return false;
		}

		public bool Has(Type type) {
			foreach (var thing in this._services) {
				if (type.IsInstanceOfType(thing)) {
					return true;
				}
			}

			return false;
		}

		/**
		* <summary>
		* Get the first service instance of a given type.
		*
		* If none could be found, a System.ArgumentException will be thrown.
		* </summary>
		* <typeparam name="T">The type to look for.</typeparam>
		* <returns>The first found instance.</returns>
		*/
		public T Get<T>()
		where T : AbstractService {
			foreach (var serv in this._services) {
				if (serv is T returner) {
					return returner;
				}
			}

			throw new ArgumentException($"The service {typeof(T).FullName} was not found.");
		}

		public AbstractService Get(Type type) {
			foreach (var serv in this._services) {
				if (type.IsInstanceOfType(serv)) {
					return serv;
				}
			}

			throw new ArgumentException($"The service {type.FullName} was not found.");
		}

		/**
		* <summary>
		* Try to get an service instance of a given type.
		* </summary>
		* <typeparam name="T">The type to look for.</typeparam>
		* <returns>An instance of that type, or null if none could be found.</returns>
		*/
		public T? TryGet<T>()
		where T : AbstractService {
			foreach (var service in this._services) {
				if (service is T returner) {
					return returner;
				}
			}

			return null;
		}

		// // You might be wondering; why can't you write it like this?:
		// // ```cs
		// // 	public T? try_get<T>()
		// // 	where T : AbstractService
		// // ```
		// // The reason for this is due to this error:
		// // `The constraints for type parameter 'T' of method 'ServiceContainer.try_get<T>()' must match the constraints for type parameter 'T' of interface method 'IObjectComponentContainer<AbstractService>.try_get<T>()'. Consider using an explicit interface implementation instead.`
		// // This is the best way, seemingly.
		// // Also, see https://stackoverflow.com/questions/1868861/how-to-call-explicit-interface-implementation-methods-internally-without-explici
		// T? IObjectComponentContainer<AbstractService>.try_get<T>()
		// where T : class {
		// 	foreach (var service in this._services) {
		// 		if (service is T returner) {
		// 			return returner;
		// 		}
		// 	}

		// 	return null;
		// }

		public AbstractService? TryGet(Type type) {
			foreach (var service in this._services) {
				if (type.IsInstanceOfType(service)) {
					return service;
				}
			}

			return null;
		}

		/**
		* <summary>
		* Get all services.
		* </summary>
		* <returns>IEnumerable</returns>
		*/
		public IEnumerable<AbstractService> GetEnumerable() {
			foreach (var service in this._services) {
				yield return service;
			}

			yield break;
		}

		/**
		* <summary>
		* Get all services of a given type.
		* </summary>
		* <typeparam name="T">The type to filter for.</typeparam>
		* <returns>Enumerable of the given type.</returns>
		*/
		public IEnumerable<T> GetEnumerable<T>()
		where T : AbstractService {
			foreach (var service in this._services) {
				if (service is T yielder) {
					yield return yielder;
				}
			}

			yield break;
		}

		public IEnumerable<AbstractService> GetEnumerable(Type type) {
			foreach (var service in this._services) {
				if (type.IsInstanceOfType(service)) {
					yield return service;
				}
			}

			yield break;
		}

		/**
		* <summary>
		* Get all services as an array.
		* </summary>
		* <returns>Array of services.</returns>
		*/
		public AbstractService[] GetAll() {
			return this.GetEnumerable().ToArray();
		}

		/**
		* <summary>
		* Get all services of a given type as an array.
		* </summary>
		* <typeparam name="T">The type to filter for.</typeparam>
		* <returns>An array of service instances of the type.</returns>
		*/
		public T[] GetAll<T>()
		where T : AbstractService {
			return this.GetEnumerable<T>().ToArray();
		}

		public AbstractService[] GetAll(Type type) {
			return this.GetEnumerable(type).ToArray();
		}

		/**
		* <summary>
		* Get all services as a list.
		* </summary>
		* <returns>A list of services.</returns>
		*/
		public List<AbstractService> GetAllAsList() {
			return new List<AbstractService>(this.GetEnumerable());
		}

		/**
		* <summary>
		* Get all services of a given type as a list.
		* </summary>
		* <typeparam name="T">The type to filter for.</typeparam>
		* <returns>A list of services of a given type.</returns>
		*/
		public List<T> GetAllAsList<T>()
		where T : AbstractService {
			return new List<T>(this.GetEnumerable<T>());
		}

		public List<AbstractService> GetAllAsList(Type type) {
			return new List<AbstractService>(this.GetEnumerable(type));
		}

		/**
		* <summary>
		* Add a new service instance of a given type.
		* </summary>
		* <typeparam name="T">The type to create.</typeparam>
		* <returns>The newly created instance.</returns>
		*/
		public T Add<T>()
		where T : AbstractService, new() {
			var inst = new T();
			this._services.Add(inst);
			return inst;
		}

		public AbstractService Add(Type type) {
			var inst = (AbstractService)Activator.CreateInstance(type);
			this._services.Add(inst);
			return inst;
		}

		/**
		* <summary>
		* Add a new service.
		* </summary>
		* <param name="instance">The instance to add.</param>
		* <returns>The provided instance.</returns>
		*/
		public AbstractService Add(AbstractService instance) {
			this._services.Add(instance);
			return instance;
		}

		/**
		* <summary>
		* Get a service, or add it if it does not exist.
		* </summary>
		* <typeparam name="T">The type of the service.</typeparam>
		* <returns>The service instance.</returns>
		*/
		public T GetOrAdd<T>()
		where T : AbstractService, new() {
			var existing = this.TryGet<T>();
			if (existing != null) {
				return existing;
			}

			return this.Add<T>();
		}

		public AbstractService GetOrAdd(Type type) {
			var existing = this.TryGet(type);
			if (existing != null) {
				return existing;
			}

			return this.Add(type);
		}

		/**
		* <summary>
		* Remove the first found service instance of a given type.
		*
		* Be careful using this as it may not get exactly what you are looking for.
		* </summary>
		* <typeparam name="T">The type to look for.</typeparam>
		*/
		public void Remove<T>()
		where T : AbstractService {
			T? remover = null;
			foreach (var thing in this._services) {
				if (thing is T remover2) {
					remover = remover2;
					break;
				}
			}

			if (remover == null) {
				throw new ArgumentException($"The service {typeof(T).FullName} could not be removed because it was not found.");
			}

			remover.Stop();
			remover.Dispose();
			this._services.Remove(remover);
		}

		public void Remove(Type type) {
			AbstractService? remover = null;
			foreach (var thing in this._services) {
				if (type.IsInstanceOfType(thing)) {
					remover = thing;
				}
			}

			if (remover == null) {
				throw new ArgumentException($"The service {type.FullName} could not be removed because it was not found.");
			}

			remover.Stop();
			remover.Dispose();
			this._services.Remove(remover);
		}

		/**
		* <summary>
		* Remove a particular service instance.
		* </summary>
		* <param name="service">The instance to remove.</param>
		*/
		public void Remove(AbstractService service) {
			service.Stop();
			service.Dispose();
			this._services.Remove(service);
		}

		/**
		* <summary>
		* Remove all service instances of a given type.
		* </summary>
		* <typeparam name="T">The type to remove.</typeparam>
		*/
		public void RemoveAllOf<T>()
		where T : AbstractService {
			// Not possible with hashset.
			// for (int i = this._services.Count - 1; i >= 0; i--) {
			// 	if (this._services[i] is T remover) {
			// 		remover.stop();
			// 		this._services.RemoveAt(i);
			// 	}
			// }

			// TODO: we could possibly reduce this to one foreach loop.
			// Create a new hashset, add items we want to keep to it, and then
			// overwrite the old hashset.
			var to_be_removed = new List<T>();
			foreach (var serv in this._services) {
				if (serv is T remover) {
					to_be_removed.Add(remover);
				}
			}

			foreach (var remover in to_be_removed) {
				this._services.Remove(remover);
				remover.Stop();
				remover.Dispose();
			}
		}

		public void RemoveAllOf(Type type) {
			// for (int i = this._services.Count - 1; i >= 0; i--) {
			// 	if (type.IsInstanceOfType(this._services[i])) {
			// 		this._services[i].stop();
			// 		this._services.RemoveAt(i);
			// 	}
			// }

			var to_be_removed = new List<AbstractService>();
			foreach (var serv in this._services) {
				if (type.IsInstanceOfType(serv)) {
					to_be_removed.Add(serv);
				}
			}

			foreach (var remover in to_be_removed) {
				this._services.Remove(remover);
				remover.Stop();
				remover.Dispose();
			}
		}

		/**
		* <summary>
		* Remove all services.
		* </summary>
		*/
		public void Clear() {
			foreach (var service in this._services) {
				service.Stop();
				service.Dispose();
			}
			this._services.Clear();
		}

		/**
		* <summary>
		* Assert that a service of a given type exists.
		*
		* Only useful in Debug mode.
		* </summary>
		* <typeparam name="T">The type to check for.</typeparam>
		*/
		public void AssertExists<T>()
		where T : AbstractService {
			#if DEBUG
				bool found = false;
				foreach (var serv in this._services) {
					if (serv is T) {
						found = true;
						break;
					}
				}

				Debug.Assert(found, $"ServiceContainer does not contain service {typeof(T).FullName}.");
			#endif
		}

		public void AssertExists(Type type) {
			#if DEBUG
				bool found = false;
				foreach (var serv in this._services) {
					if (type.IsInstanceOfType(serv)) {
						found = true;
						break;
					}
				}

				Debug.Assert(found, $"ServiceContainer does not contain service {type.FullName}.");
			#endif
		}

		/**
		* <summary>
		* Assert that a service exists.
		*
		* Only useful in Debug mode.
		* </summary>
		* <typeparam name="T">The type to check for.</typeparam>
		*/
		public void AssertNotExists<T>()
		where T : AbstractService {
			#if DEBUG
				bool found = false;
				foreach (var serv in this._services) {
					if (serv is T) {
						found = true;
						break;
					}
				}

				Debug.Assert(!found, $"ServiceContainer contains service {typeof(T).FullName}.");
			#endif
		}

		public void AssertNotExists(Type type) {
			#if DEBUG
				bool found = false;
				foreach (var serv in this._services) {
					if (type.IsInstanceOfType(serv)) {
						found = true;
						break;
					}
				}

				Debug.Assert(!found, $"ServiceContainer contains service {type.FullName}.");
			#endif
		}

		/**
		* <summary>
		* Require that a particular type of service exists.
		*
		* If it does, we will get it. If not, a System.ArgumentException will occur.
		*
		* If multiple instances exist, it will return the first found instance.
		* </summary>
		* <typeparam name="T">The type to check for.</typeparam>
		* <returns>The service instance.</returns>
		*/
		public T Require<T>()
		where T : AbstractService {
			return this.Get<T>();
		}

		public AbstractService Require(Type type) {
			return this.Get(type);
		}

		/**
		* <summary>
		* Ask that a particular type of service exists.
		*
		* If it does, it will be returned. If not, null is returned.
		*
		* If multiple instances exist, it will return the first found instance.
		* </summary>
		* <typeparam name="T"></typeparam>
		* <returns></returns>
		*/
		public T? Import<T>()
		where T : AbstractService {
			return this.TryGet<T>();
		}

		public AbstractService? Import(Type type) {
			return this.TryGet(type);
		}

		/**
		* <summary>
		* Start all service instances.
		* </summary>
		*/
		public void Start() {
			foreach (var serv in this._services) {
				serv.Start();
			}
		}

		/**
		* <summary>
		* Stop and destroy all service instances.
		* </summary>
		*/
		public void Stop() {
			this.Clear();
		}

		/**
		* <summary>
		* Fire a process event, used for animation and frames.
		* </summary>
		*/
		public void Process() {
			this.ProcessEvent?.Invoke();
		}

		/**
		* <summary>
		* Fire a physics process event, used for physics and general logic.
		* </summary>
		* <param name="delta"></param>
		*/
		public void PhysicsProcess(float delta) {
			this.PhysicsProcessEvent?.Invoke(delta);
		}

		/**
		* <summary>
		* Fire an input event, used for handling user input.
		* </summary>
		* <param name="evt"></param>
		*/
		public void Input(Godot.InputEvent evt) {
			this.InputEvent?.Invoke(evt);
		}

		public void Dispose() {
			this._dispose();
			GC.SuppressFinalize(this);
		}

		private void _dispose() {
			if (this._disposed) {
				return;
			}
			this._disposed = true;

			this.Clear();
		}

	}
}
