namespace Godasum.Util.Objects {
	public interface IObjectComponent
	{

		/**
		* <summary>
		* Initialize the component.
		*
		* You should ask for other components here.
		* </summary>
		*/
		void Init();

		/**
		* <summary>
		* Start the component.
		* </summary>
		*/
		void Start();

		/**
		* <summary>
		* Stop the component.
		* </summary>
		*/
		void Stop();

		/**
		* <summary>
		* Handle animation and other things that happen every frame.
		* </summary>
		*/
		void Process();

		/**
		* <summary>
		* Handle physics and other things that happen every tick.
		* </summary>
		* <param name="delta"></param>
		*/
		void PhysicsProcess(float delta);

		/**
		* <summary>
		* Handle input events sent by the player.
		* </summary>
		* <param name="evt"></param>
		*/
		void Input(Godot.InputEvent evt);

		/**
		* <summary>
		* Subscribe to our container's process event.
		* </summary>
		*/
		void SubscribeProcess();

		/**
		* <summary>
		* Unsubscribe to our container's process event.
		* </summary>
		*/
		void UnsubscribeProcess();

		/**
		* <summary>
		* Subscribe to our container's physics process event.
		* </summary>
		*/
		void SubscribePhysicsProcess();

		/**
		* <summary>
		* Unsubscribe to our container's physics process event.
		* </summary>
		*/
		void UnsubscribePhysicsProcess();

		/**
		* <summary>
		* Subscribe to our container's input event.
		* </summary>
		*/
		void SubscribeInput();

		/**
		* <summary>
		* Unsubscribe to our container's input event.
		* </summary>
		*/
		void UnsubscribeInput();

	}
}
