namespace Godasum.Util.Objects {
	using System.Diagnostics;
	using System;

	public interface IObjectComponentContainer<TDerived>
	where TDerived : class, IObjectComponent
	{

		bool Has<T>()
		where T : TDerived;

		bool Has(Type type);

		T Get<T>()
		where T : TDerived;

		TDerived Get(Type type);

		// Note: T is nullable!
		// However, we cannot display it here due to C#.
		// Attempting to set it to `T?` will ask you to constrain T to
		// a nullable reference type. You could try `where T : class, TDerived`.
		// However, implementing classes cannot implement it correctly then.
		// For example, AspectContainer cannot implement it due to
		// not being to specify `where T : class, AbstractAspect`.
		T TryGet<T>()
		where T : TDerived;

		TDerived? TryGet(Type type);

		T Add<T>()
		where T : TDerived, new();

		TDerived Add(Type type);

		T GetOrAdd<T>()
		where T : TDerived, new();

		TDerived GetOrAdd(Type type);

		void Remove<T>()
		where T : TDerived;

		void Remove(Type type);

		void Clear();

		void AssertExists<T>()
		where T : TDerived;

		void AssertNotExists<T>()
		where T : TDerived;

		void Start();

		void Stop();

		void Process();

		void PhysicsProcess(float delta);

		void Input(Godot.InputEvent evt);

	}
}
