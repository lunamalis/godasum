namespace Godasum.Util {
	using System.Collections.Generic;
	using System;

	public static class ListUtil {

		public static List<T> ShiftRight<T>(List<T> list) {
			if (list.Count < 2) {
				throw new ArgumentOutOfRangeException("List is too small to shift.");
			}

			var last = list[list.Count - 1];

			list.RemoveAt(list.Count - 1);
			list.Insert(0, last);
			return list;

		}

		public static List<T> ShiftLeft<T>(List<T> list) {
			if (list.Count < 2) {
				throw new ArgumentOutOfRangeException("List is too small to shift.");
			}

			var first = list[0];

			list.RemoveAt(0);
			list.Insert(list.Count - 1, first);

			return list;
		}
	}
}
