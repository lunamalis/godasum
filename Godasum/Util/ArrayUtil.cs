namespace Godasum.Util {
	using System;

	public static class ArrayUtil {
		public static T[] GetAllButLast<T>(T[] arr) {
			var arr_new = new T[arr.Length - 1];

			for (int i = 0; i < (arr.Length - 1); i++) {
				arr_new[i] = arr[i];
			}

			return arr_new;
		}

		public static T[] GetAllButLastN<T>(T[] arr, int n) {
			if (n < 1 || n > (arr.Length - 1)) {
				throw new ArgumentOutOfRangeException($"n is outside range of array. n: {n}, array length: {arr.Length} - 1");
			}

			var arr_new = new T[arr.Length - n];
			for (int i = 0; i < (arr.Length - n); i++) {
				arr_new[i] = arr[i];
			}

			return arr_new;
		}

	}
}
