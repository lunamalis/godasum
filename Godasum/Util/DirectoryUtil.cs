namespace Godasum.Util {
	using System.Collections.Generic;
	using Godot;

	public static class DirectoryUtil {

		public static List<string> ListDirectories(string path) {
			return new List<string>(System.IO.Directory.GetDirectories(path));
		}

		public static List<string> ListDirectoriesGodot(string path, bool skip_hidden = true) {
			var dirs = new List<string>();

			using (var dir_search = new Directory()) {
				dir_search.Open(path);
				dir_search.ListDirBegin();
				while (true) {
					var testee = dir_search.GetNext();
					if (testee == string.Empty) {
						// Reached the end of the line.
						break;
					}

					// Only get directories.
					if (!dir_search.CurrentIsDir()) {
						continue;
					}

					// Skip files beginning with a dot.
					// Also skips the directories `.` and `..`.
					// Which refer to the current and parent directories, respectively.
					if (skip_hidden && testee.BeginsWith(".")) {
						continue;
					}

					dirs.Add(testee);
				}
				dir_search.ListDirEnd();

				return dirs;
			}
		}
	}
}
