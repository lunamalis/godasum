namespace Godasum.Logging {
	internal static class LogManager
	{

		internal static void setup() {
			var config = new NLog.Config.LoggingConfiguration();

			var layout = _layout;

			var logpath = Godot.ProjectSettings.GlobalizePath("user://logs/core-log.txt");

			// Ignore disposing of these objects; we never really dispose of them.
#pragma warning disable IDE0067

			var log_console = new NLog.Targets.ConsoleTarget("log_console") {
				Layout = layout
			};

			var log_file = new NLog.Targets.FileTarget() {
				FileName = logpath,
				Layout = layout,
				LineEnding = NLog.Targets.LineEndingMode.LF,
				// Godot should create the logs directory before this.
				CreateDirs = false,
				FileAttributes = NLog.Targets.Win32FileAttributes.ReadOnly,
				KeepFileOpen = true,
				ArchiveFileName = _filename_layout,
				ArchiveNumbering = NLog.Targets.ArchiveNumberingMode.Sequence,
				ArchiveEvery = NLog.Targets.FileArchivePeriod.Day,
				ArchiveOldFileOnStartup = true,
				// TODO: a lot of this might be configurable through user settings.
				MaxArchiveFiles = 100
			};

			var log_gd = new _GDTarget() {
				Layout = layout
			};

#pragma warning restore IDE0067

			config.AddRule(NLog.LogLevel.Info, NLog.LogLevel.Fatal, log_console);
			config.AddRule(NLog.LogLevel.Debug, NLog.LogLevel.Fatal, log_file);

			// Only print in the Godot editor.
#if TOOLS
			config.AddRule(NLog.LogLevel.Debug, NLog.LogLevel.Fatal, log_gd);
#endif

			NLog.LogManager.Configuration = config;

		}

		private static NLog.Layouts.Layout _layout {
			get {
				const string str = @"${date:format=yyyy-MM-dd HH\:mm\:ss} [${logger}/${level}] ${message}  ${exception}";
				return NLog.Layouts.SimpleLayout.FromString(
					str,
					throwConfigExceptions: true
				);
			}
		}

		private static NLog.Layouts.Layout _filename_layout {
			get {
				const string str = @"core-log_${date:format=yyyy-MM-dd}_{#}.txt";
				return NLog.Layouts.SimpleLayout.FromString(
					Godot.ProjectSettings.GlobalizePath($"user://logs/{str}"),
					throwConfigExceptions: true
				);
			}
		}

		private sealed class _GDTarget : NLog.Targets.TargetWithLayout
		{
			// Only really meant for Godot editor.

			protected override void Write(NLog.LogEventInfo log_event) {

				var message = this.Layout.Render(log_event);

				switch (log_event.Level.Ordinal) {
					// Highest to lowest
					case int n when n >= NLog.LogLevel.Error.Ordinal:
						Godot.GD.PushError(message);
						break;
					case int n when n >= NLog.LogLevel.Warn.Ordinal:
						Godot.GD.PushWarning(message);
						break;
				}

				Godot.GD.Print(message);
			}
		}

	}
}
