namespace Godasum.Modloader {
	using System.Reflection;
	using System;
	using System.Collections.Generic;
	using Godot;

	public class ModLoader {

		internal Dictionary<string, Modding.Mod> mods = new Dictionary<string, Modding.Mod>();
		// public Dictionary<string, Godasum.objects.Entity> entities = new Dictionary<string, Godasum.objects.Entity>();

		internal Dictionary<string, Type> entity_types = new Dictionary<string, Type>();
		internal Dictionary<string, Type> ship_types = new Dictionary<string, Type>();
		internal Dictionary<string, Type> projectile_types = new Dictionary<string, Type>();

		private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

		internal void load_all() {
			this.load_all_in_dir(ProjectSettings.GlobalizePath("res://mods"));
			this.load_all_in_dir(ProjectSettings.GlobalizePath("user://mods"));
			this.load_all_in_dir(System.IO.Path.Combine($"{OS.GetExecutablePath().GetBaseDir()}", "mods"));

			foreach (var modpair in this.mods) {
				var mod = modpair.Value;
				mod.Setup();
			}
		}

		internal void load_all_in_dir(string path) {
			log.Info($"Searching {path}");
			if (System.IO.Path.GetFileName(System.IO.Path.GetDirectoryName(path)).BeginsWith("Godot_v")) {
				// We are in debug mode: do not make a mods directory here.
				log.Info("Skipping Godot debug-mode directory.");
				return;
			}
			// First, find all directories.
			if (!System.IO.Directory.Exists(path)) {
				System.IO.Directory.CreateDirectory(path);
			}
			var dirs = Util.DirectoryUtil.ListDirectories(path);
			// TODO: Then, find all .zip files.

			foreach (var d in dirs) {
				log.Info("Trying directory {0}/{1}", path, d);
				var dname = System.IO.Path.GetFileName(d.TrimEnd(System.IO.Path.DirectorySeparatorChar));
				if (this._dir_loadable(d)) {
					log.Info("Loading mod: `{0}`: `{1}`", dname, d);
					this.load_mod(dname, d);
				}
			}

			log.Info("Finished searching directory for mods. Mod count is now: {0}.", this.mods.Count);
		}

		internal void load_mod(string name, string path) {
			log.Info("Loading mod {0} from {1}", name, path);

			var modinfo = this.read_modinfo(name, $"{path}/modinfo.json");

			var mod_class = this._get_mod_class(name, path, modinfo);
			if (mod_class == null) {
				log.Error("Error loading mod: returned Mod class is null.");
				return;
			}

			ProjectSettings.LoadResourcePack($"{path}/data.zip");

			mod_class.Init(this);
			this.mods.Add(mod_class.MODID, mod_class);
		}

		internal Modding.ModInfo read_modinfo(string name, string path) {
			var file_search = new Godot.File();
			if (!file_search.FileExists(path)) {
				throw new ArgumentException($"No modinfo.json found for mod {name}.");
			}

			file_search.Open(path, File.ModeFlags.Read);

			Modding.ModInfo modinfo;

			try {
				modinfo = Newtonsoft.Json.JsonConvert.DeserializeObject<Modding.ModInfo>(file_search.GetAsText());
			}
			finally {
				file_search.Close();
			}

			return modinfo;
		}

		public void RegisterEntity<T>(string entity_id)
		where T : Godasum.Objects.Battle.EntityBO, new() {
			log.Info("Registering entity with id: {0}, {1}", entity_id, typeof(T));
			this.entity_types.Add(entity_id, typeof(T));
		}

		// public void register_ship<T>(string entity_id)
		// where T : Godasum.objects.Ship, new() {
		// 	GD.Print($"Registering SHIP with ID {entity_id}, {typeof(T)}");
		// 	this.ship_types.Add(entity_id, typeof(T));
		// }

		// public void register_projectile<T>(string entity_id)
		// where T : Godasum.objects.Projectile, new() {
		// 	this.projectile_types.Add(entity_id, typeof(T));
		// }

		private bool _dir_loadable(string path) {
			if (path.EndsWith(".disabled")) {
				return false;
			}

			if (System.IO.File.Exists(
				System.IO.Path.Combine(path, "modinfo.json")
			)) {
				return true;
			}

			return false;
		}

		private string _get_dll_path(string name, string path) {
			return System.IO.Path.Combine(path, "mono", $"{name}.dll");
		}

		private Assembly? _get_dll(string name, string path, Modding.ModInfo modinfo) {
			var dll_path = this._get_dll_path(modinfo.cname, path);
			try {
				return Assembly.LoadFile(dll_path);
			}
			catch (System.ArgumentException e) {
				log.Error("Error loading mod dll file (should be an absolute path).");
				log.Error("Path: {0}", dll_path);
				log.Error("Error: {0}", e.Message);
			}
			catch (System.IO.FileLoadException e) {
				log.Error("Could not load mod .dll file.");
				log.Error("Error: {0}", e.Message);
			}
			catch (System.BadImageFormatException e) {
				log.Error("Could not load .dll due to it being an invalid assembly, or the dotnet runtime used to compile it was made for a later version.");
				log.Error("Error: {0}", e.Message);
			}
			catch (System.NotSupportedException e) {
				log.Error("dotnet securityEvidence failure; cannot load .dll.");
				log.Error("Error: {0}", e.Message);
			}
			catch (System.IO.FileNotFoundException e) {
				log.Error("Could not load .dll due to it not being found or it being an invalid image.");
				log.Error("Path: {0}", dll_path);
				log.Error("Error: {0}", e.Message);
			}
			return null;
		}

		private Godasum.Modding.Mod? _get_mod_class(string name, string path, Modding.ModInfo modinfo) {
			var dll = this._get_dll(name, path, modinfo);
			if (dll == null) {
				log.Error("Will not try loading mod class due to returned null dll.");
				return null;
			}

			var entry_path = $"{modinfo.cname}.Mod";

			Type? entry_type = null;
			try {
				entry_type = dll.GetType(entry_path);
			}
			catch (System.IO.FileNotFoundException e) {
				log.Error("Mod could not load due to no Mod class existing.");
				log.Error("Error: {0}", e.Message);
			}
			catch (System.BadImageFormatException e) {
				log.Error("Mod could not load due to an invalid DLL Assembly. This may be due to using later versions of the dotnet runtime.");
				log.Error("Error: {0}", e.Message);
			}

			return Activator.CreateInstance(entry_type) as Godasum.Modding.Mod;
		}

	}
}
