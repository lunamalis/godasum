namespace Godasum.Config {
	public class ConfigManager {

		public static readonly string RootDirectory = Godot.ProjectSettings.GlobalizePath("user://config");

		public readonly UserConfig user = new UserConfig();

		private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

		public void load_all() {
			log.Info("Loading all configs.");
			// If this is our first run, the config directory won't exist.
			System.IO.Directory.CreateDirectory(RootDirectory);

			this.user.Load();
		}

		public void apply_all() {
			log.Info("Applying all configs.");
			this.user.Apply();
		}

	}
}
