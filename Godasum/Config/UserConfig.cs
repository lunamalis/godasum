namespace Godasum.Config {
	using Nett;

	public class UserConfig : AbstractConfig<UserConfigSettings> {


		public override string Path { get { return "user.json"; } }

		public override void Apply() {
			Godot.OS.WindowSize = this.Settings.Display.Dimensions;
			Godot.OS.WindowBorderless = this.Settings.Display.Borderless;
			Godot.OS.WindowFullscreen = this.Settings.Display.Fullscreen;
		}

	}

}
