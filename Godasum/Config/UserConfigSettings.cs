namespace Godasum.Config {
	using System;
	using Newtonsoft.Json;

	[Serializable]
	[JsonObject(MemberSerialization.OptOut, ItemRequired=Required.Always)]
	public class UserConfigSettings
	{

		public DisplayCategory Display = new DisplayCategory();

		[Serializable]
		[JsonObject(MemberSerialization.OptOut, ItemRequired=Required.Always)]
		public class DisplayCategory
		{
			public Godot.Vector2 Dimensions = new Godot.Vector2(1024, 600);

			public bool Borderless = false;

			public bool Fullscreen = false;
		}
	}
}
