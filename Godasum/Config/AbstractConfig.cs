namespace Godasum.Config {
	using System;

	public abstract class AbstractConfig<TTemplate>
	where TTemplate : class, new()
	{

		public static readonly string RootDirectory = ConfigManager.RootDirectory;

		public abstract string Path { get; }

#if DEBUG
		public TTemplate Settings {
			get {
				if (this._settings == null) {
					throw new InvalidOperationException("The config settings were not loaded.");
				}
				return this._settings;
			}

			set {
				this._settings = value;
			}
		}

		private TTemplate? _settings;
#else
		public TTemplate Settings;
#endif

		public virtual void Save() {
			this._Save(this.Settings);
		}

		public virtual void Load() {
			this._WriteDefaultIfUnset();

			var data = System.IO.File.ReadAllText(this._Path);

			this.Settings = Newtonsoft.Json.JsonConvert.DeserializeObject<TTemplate>(data);

		}

		public abstract void Apply();

		protected string _Path {
			get {
				return System.IO.Path.Combine(RootDirectory, this.Path);
			}
		}

		protected void _WriteDefaultIfUnset() {
			if (System.IO.File.Exists(this._Path)) {
				return;
			}

			var default_instance = new TTemplate();

			this._Save(default_instance);

		}

		protected void _Save(TTemplate settings) {
			var text = Newtonsoft.Json.JsonConvert.SerializeObject(
				settings,
				typeof(TTemplate),
				new Newtonsoft.Json.JsonSerializerSettings() {
					Formatting = Newtonsoft.Json.Formatting.Indented
				}
			);

			System.IO.File.WriteAllText(this._Path, text);
		}

	}
}
