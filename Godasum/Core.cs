namespace Godasum {
	using System.Reflection;
	using System;
	using Godot;

	public static class Core {

		internal static readonly Godasum.Modloader.ModLoader modloader = new Godasum.Modloader.ModLoader();

		internal static readonly Godasum.Config.ConfigManager configs = new Godasum.Config.ConfigManager();

		private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

		public static bool Loaded { get; private set; }

		public static Util.CoreRegistry Registry { get; } = new Util.CoreRegistry();

		static Core() {
			Logging.LogManager.setup();
			log.Info("Created log.");
		}

		internal static void initialize() {
			if (Core.Loaded) {
				throw new ApplicationException("Core already initialized!");
			}
			Core.Loaded = true;

			log.Info("Initializing core.");

			// VersionHandler.init();
			log.Info("Godasum version: {0}", VersionHandler.FullVersion);

			AppDomain.CurrentDomain.UnhandledException += Core.unhandled_exception;

			configs.load_all();
			// We can't actually apply configs here, because then it tries to apply
			// certain settings (such as the video resolution) to the debugger.
			// Instead, this is applied at the title screen.
			// configs.apply_all();
		}

		internal static void unhandled_exception(object sender, UnhandledExceptionEventArgs e) {
			log.Error($"EXCEPTION: {e.ExceptionObject}");
			GD.PrintS($"EXCEPTION: {e.ExceptionObject}");
			GD.PrintErr("Quitting.");
		}

	}
}
