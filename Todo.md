# Todo

* Move from Msbuild (VS Build Tools) to Msbuild (Mono)
  * (See Godot -> Editor Settings -> Mono -> Builds)
* Completely revamp configurations
* Multiplayer
  * Godot has a lot of utilities useful for this
  * May require a large amount of restructing
  * Server-side ...
    * Server-side mod lists
      * Host on Github/Gitlab an official mod repo
      * Also allow unofficial mod repos
      * Server should send mod info to clients, who download them from the repo
    * Server-side saves
    * Server-side configs
      * Mods should be able to set user-configs that do not need to be synced
* Lasers with sprites
  * See 9-patch, [example](https://discordapp.com/channels/294511987684147212/422469313283489802/716665947565588480)
* Component-based entity system
* Battle system: all objects should extend from very simple `BattleObject`
* Battle structures
* Mod packing; mods should be distributed by .zip
  * Unpacking should be unnecessary
* Control scheme
