# Godasum

## What

This is an open-source spaceship combat game.

## Compiling

Godasum is made with Godot and C#.

To compile, you will need the following things:

* [.NET Framework version 4.8 (direct download)](https://dotnet.microsoft.com/download/dotnet-framework/thank-you/net48-developer-pack-offline-installer)
* [Godot Mono, version 3.2.1 (direct win64 download)](https://downloads.tuxfamily.org/godotengine/3.2.1/mono/Godot_v3.2.1-stable_mono_win64.zip)

To build, you will need to open the project in Godot. Then, press F5 to debug the project. You can use the Export
feature to export as an executable.

## License

Licensed under the [Apache-2.0 License](LICENSE.md).
