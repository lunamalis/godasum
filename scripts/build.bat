SETLOCAL ENABLEEXTENSIONS
SET me=%~n0
SET parent=%~dp0

SET buildtools_path="C:\Program Files (x86)\Microsoft Visual Studio\2019\Buildtools"

REM DO NOT EDIT AFTER THIS LINE.

REM The tilde thing removes the quotation marks. Yes, this is necessary...
SET BUILDTOOLS_EXE="%buildtools_path:~1,-1%\MSBuild"
%BUILDTOOLS_EXE%
